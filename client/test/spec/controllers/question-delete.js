'use strict';

describe('Controller: QuestionDeleteCtrl', function () {

  // load the controller's module
  beforeEach(module('ModelierApp'));

  var QuestionDeleteCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    QuestionDeleteCtrl = $controller('QuestionDeleteCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(QuestionDeleteCtrl.awesomeThings.length).toBe(3);
  });
});
