'use strict';

/**
 * @ngdoc service
 * @name ModelierApp.question
 * @description
 * # question
 * Factory in the ModelierApp.
 */
angular.module('ModelierApp')
  .factory('Question', function (DataRestangular) {
    return DataRestangular.service('question');
  });
