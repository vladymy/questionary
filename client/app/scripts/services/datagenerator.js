'use strict';

/**
 * @ngdoc service
 * @name mDatagenerator.datagenerator
 * @description
 * # datagenerator
 * Factory in the mDatagenerator.
 */
 angular.module('mDatagenerator', [
  'ngMaterial'
]);
angular.module('mDatagenerator')
  .factory('datagenerator', function(Question) {
    var questionData = [];

    var _getQuestionData = function() {
      
      //SKELETOND DATA
      // questionData = [
      //   {
      //     title: 'Выберите модель',
      //     description: 'question description',
      //     type: 'select',
      //     option: 'single',
      //     passed: false,
      //     disabled: false,
      //     answers: [
      //       {
      //         title: 'answer1',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer2',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer3',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer4',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer5',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer6',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       }
      //     ]
      //   },
      //   {
      //     title: 'Форма носочной части',
      //     description: 'question description',
      //     type: 'select',
      //     option: 'single',
      //     passed: false,
      //     disabled: true,
      //     answers: [
      //       {
      //         title: 'answer1',
      //         description: 'answer description',
      //         img: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer2',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer3',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       }
      //     ]
      //   },
      //   {
      //     title: 'Оттенки лака, кожи и замши',
      //     description: 'question description',
      //     type: 'select',
      //     option: 'single',
      //     passed: false,
      //     disabled: true,
      //     answers: [
      //       {
      //         title: 'answer1',
      //         description: 'answer description',
      //         img: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer2',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer3',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       }
      //     ]
      //   },
      //   {
      //     title: 'Выберите цвет кожи',
      //     description: 'question description',
      //     type: 'select',
      //     option: 'single',
      //     passed: false,
      //     disabled: true,
      //     answers: [
      //       {
      //         title: 'answer1',
      //         description: 'answer description',
      //         img: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer2',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer3',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       }
      //     ]
      //   },
      //   {
      //     title: 'Выберите размер обуви',
      //     description: 'question description',
      //     type: 'select',
      //     option: 'single',
      //     passed: false,
      //     disabled: true,
      //     answers: [
      //       {
      //         title: 'answer1',
      //         description: 'answer description',
      //         img: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer2',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer3',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       }
      //     ]
      //   },
      //   {
      //     title: 'Контакты',
      //     description: 'question description',
      //     type: 'select',
      //     option: 'single',
      //     passed: false,
      //     disabled: true,
      //     answers: [
      //       {
      //         title: 'answer1',
      //         description: 'answer description',
      //         img: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer2',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer3',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       }
      //     ]
      //   },
      //   {
      //     title: 'Оплата',
      //     description: 'question description',
      //     type: 'select',
      //     option: 'single',
      //     passed: false,
      //     disabled: true,
      //     answers: [
      //       {
      //         title: 'answer1',
      //         description: 'answer description',
      //         img: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer2',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       },
      //       {
      //         title: 'answer3',
      //         description: 'answer description',
      //         imgUrl: 'url',
      //         selected: false
      //       }
      //     ]
      //   }
      // ];
      questionData = Question.getList().$object;

      return questionData;
    };
    // Public API here
    return {
      getQuestionData: _getQuestionData
    };
  });