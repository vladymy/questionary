'use strict';

/**
 * @ngdoc function
 * @name ModelierApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ModelierApp
 */
angular.module('ModelierApp')
  .controller('MainCtrl', function ($scope, $mdDialog, datagenerator) {
  	
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });