'use strict';

/**
 * @ngdoc function
 * @name ModelierApp.controller:QuestionEditCtrl
 * @description
 * # QuestionEditCtrl
 * Controller of the ModelierApp
 */
angular.module('ModelierApp')
  .controller('QuestionEditCtrl', function ($scope, Question, $routeParams, $location, $mdDialog) {
    $scope.question = {
    	title: '',
      description: '',
      option: ''
    };
    $scope.questionOptions = [
    	'single',
    	'multi'
    ];
    $scope.goBack = function() {
    	$location.path('/admin/questions/');
    };
    $scope.addAnswer = function() {
    	$scope.question.answers.push({
        title: 'answer' + ($scope.question.answers.length + 1),
        description: 'answer description',
        imgUrl: 'url'
      });
    };
    $scope.isValid = function() {
    	var notValid;	
    	function checklist(list) {
				_.each(list, function(item) {
	    		if (item === '') {
	    			notValid = true;
	    		}
	    	});
    	}
    	checklist($scope.question);
    	_.each($scope.question.answers, function(answerObj) {
    		checklist(answerObj);
    	});
    	return !notValid;
    };
    $scope.saveData = function(ev) {
    	console.log($scope.question);
    	if(!$scope.isValid()) {
    		$mdDialog.show(
		      $mdDialog.alert()
		        .clickOutsideToClose(true)
		        .title('Validation Error')
		        .textContent('You should fill all requered inputs.')
		        .ok('Got it!')
		        .targetEvent(ev)
		    );
    	} else {
	      $scope.question.save().then(function() {
	        $scope.goBack();
	      });
	    }
    };
	  Question.one($routeParams.id).get().then(function(question) {
	    $scope.question = question;
	  });
	  $scope.deleteAnswer = function(ev, answer, answerIndex) {
			// Appending dialog to document.body to cover sidenav in docs app
			var confirm = $mdDialog.confirm()
						.title('Are you sure want delete ' + answer.title + '?')
						.targetEvent(ev)
						.ok('Please do it!')
						.cancel('Oops.. No! Dont do it!');
			$mdDialog.show(confirm).then(function() {
				$scope.question.answers.splice(answerIndex, 1);
			});
		};
  });
