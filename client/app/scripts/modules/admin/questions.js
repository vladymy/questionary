'use strict';

/**
 * @ngdoc function
 * @name ModelierApp.controller:QuestionsCtrl
 * @description
 * # QuestionsCtrl
 * Controller of the ModelierApp
 */
angular.module('ModelierApp')
	.controller('QuestionsCtrl', function ($scope, Question, $location, $route, $mdDialog) {
		$scope.questions = Question.getList().$object;
		$scope.selected = [];
		$scope.addQuestion = function() {
			$location.path('/admin/add-question/');
		};
		$scope.deleteQuestion = function(ev, question) {
			// Appending dialog to document.body to cover sidenav in docs app
			var confirm = $mdDialog.confirm()
						.title('Are you sure want delete ' + question.title + '?')
						.targetEvent(ev)
						.ok('Please do it!')
						.cancel('Oops.. No! Dont do it!');
			$mdDialog.show(confirm).then(function() {
				question.remove().then(function() {
		      $route.reload();
		    });
			});
		};
	});
