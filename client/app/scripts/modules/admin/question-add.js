'use strict';

/**
 * @ngdoc function
 * @name ModelierApp.controller:AdminAddQuestionCtrl
 * @description
 * # AdminAddQuestionCtrl
 * Controller of the ModelierApp
 */
angular.module('ModelierApp')
  .controller('QuestionAddCtrl', function ($location, Question, $scope, $mdDialog) {
    $scope.question = {
    	title: '',
      description: '',
      option: ''
    };
    $scope.question.answers = [{
      title: '',
      description: ''
    }];
    $scope.questionOptions = [
    	'single',
    	'multi'
    ];
    $scope.goBack = function() {
    	$location.path('/admin/questions/');
    };

    $scope.isValid = function() {
    	var notValid;	
    	function checklist(list) {
				_.each(list, function(item) {
	    		if (item === '') {
	    			notValid = true;
	    		}
	    	});
    	}
    	checklist($scope.question);
    	_.each($scope.question.answers, function(answerObj) {
    		checklist(answerObj);
    	});
    	return !notValid;
    };	
    $scope.saveQuestion = function(ev) {
    	$scope.question.type = 'select';
    	if(!$scope.isValid()) {
    		$mdDialog.show(
		      $mdDialog.alert()
		        .clickOutsideToClose(true)
		        .title('Validation Error')
		        .textContent('You should fill all requered inputs.')
		        .ok('Got it!')
		        .targetEvent(ev)
		    );
    	} else {
		    Question.post($scope.question).then(function() {
		      $scope.goBack();
		    });
		  }
	  };
	  $scope.addAnswer = function() {
    	$scope.question.answers.push({
        title: 'answer' + ($scope.question.answers.length + 1),
        description: 'answer description',
        imgUrl: 'url'
      });
    };    
    $scope.deleteAnswer = function(ev, answer, answerIndex) {
			// Appending dialog to document.body to cover sidenav in docs app
			var confirm = $mdDialog.confirm()
						.title('Are you sure want delete answer?')
						.targetEvent(ev)
						.ok('Please do it!')
						.cancel('Oops.. No! Dont do it!');
			$mdDialog.show(confirm).then(function() {
				$scope.question.answers.splice(answerIndex, 1);
			});
		};
  });
