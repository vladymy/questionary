'use strict';

/**
 * @ngdoc function
 * @name ModelierApp.controller:constructorCtrl
 * @description
 * # constructorCtrl
 * Controller of the ModelierApp
 */

angular.module('mConstructor', [
  'ngMaterial',
  'mDatagenerator'
])
  .controller('constructorCtrl', function ($scope, $mdDialog, datagenerator, $timeout) {
    $scope.questionData = datagenerator.getQuestionData();
    $scope.selectedTab = 0;
    $scope.progressBar = {
      value: 0
    };

    $scope.selectAnswer = function (answer) {
      answer.selected = !answer.selected;
      $scope.checkQuestionStatus();
    };
    $scope.isDisabledTab = function(question, index) {
      return (($scope.selectedTab !== index) && !question.passed && !question.enabled);
    };
    $scope.checkQuestionStatus = function () {
      _.each($scope.questionData, function (question, index) {
        if ($scope.selectedTab === index) {
          $scope.selectedTab = index;
          if (_.findWhere(question.answers, {selected: true})) {
            question.passed = true;
            if($scope.questionData[index+1]) {
              $scope.questionData[index+1].enabled = true;
            }
            if (question.option === 'single') {
              $timeout(function() {
                $scope.selectedTab = $scope.selectedTab + 1;
              });
            }
          } else {
            question.passed = false;
          }
        }
      });
      $scope.updateProgressBar();
    };
    $scope.updateProgressBar = function () {
      var passedQuestions = 0;
      _.each($scope.questionData, function (question) {
        if (question.passed) {
          passedQuestions++;
        }
      });
      $scope.progressBar.value = Math.round((passedQuestions * 100) / $scope.questionData.length);
    };
  });
