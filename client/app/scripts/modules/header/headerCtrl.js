'use strict';

/**
 * @ngdoc function
 * @name headerModule.controller:MainCtrl
 * @description
 * # HeaderCtrl
 * Controller of the headerModule
 */
angular.module('mHeader', [
	'ngMaterial'
])

angular.module('mHeader').controller('HeaderCtrl', function ($scope, $mdDialog) {
    $scope.showDialog = function(ev, name) {
	    $mdDialog.show({
	      controller: DialogController,
	      templateUrl: 'scripts/modules/main/views/'+name+'.html',
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      clickOutsideToClose:true
	    })
	        .then(function(answer) {
	          $scope.status = 'You said the information was "' + answer + '".';
	        }, function() {
	          $scope.status = 'You cancelled the dialog.';
	        });
	  };
  });

function DialogController($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
}