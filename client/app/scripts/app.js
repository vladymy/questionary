'use strict';

/**
 * @ngdoc overview
 * @name ModelierApp
 * @description
 * # ModelierApp
 *
 * Main module of the application.
 */
angular
  .module('ModelierApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngMaterial',
    'md.data.table',
    'mConstructor',
    'mHeader',
    'restangular'
  ])
  .config(function ($routeProvider, RestangularProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'scripts/modules/main/views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/constructor', {
        templateUrl: 'scripts/modules/constructor/views/constructor.html',
        controller: 'constructorCtrl',
        controllerAs: 'constructor'
      })

      .when('/admin/add-question', {
        templateUrl: 'scripts/modules/admin/views/question-add.html',
        controller: 'QuestionAddCtrl'
      })
      .when('/admin/question/:id/edit', {
        templateUrl: 'scripts/modules/admin/views/question-edit.html',
        controller: 'QuestionEditCtrl'
      })
      .when('/admin/question/:id/delete', {
        templateUrl: 'scripts/modules/admin/views/question-delete.html',
        controller: 'QuestionDeleteCtrl'
      })
      .when('/admin/questions', {
        templateUrl: 'scripts/modules/admin/views/questions.html',
        controller: 'QuestionsCtrl'
      })
      .when('/admin', {
        templateUrl: 'scripts/modules/admin/views/questions.html',
        controller: 'QuestionsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
    RestangularProvider.setBaseUrl('http://localhost:3000');
  })
  .config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('blue-grey')
      .accentPalette('deep-orange')  
      .warnPalette('red');
  })
  .factory('DataRestangular', function(Restangular) {
    return Restangular.withConfig(function(RestangularConfigurer) {
      RestangularConfigurer.setRestangularFields({
        id: '_id'
      });
    });
  })
  .filter('trusted', function ($sce) {
    return function(url) {
      return $sce.trustAsResourceUrl(url);
    };
  });