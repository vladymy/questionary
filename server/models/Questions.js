var mongoose = require('mongoose');

// Create the MovieSchema.

var QuestionSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  option: {
    type: String,
    required: true
  },
  answers: [
    {
      title: {
        type: String,
        required: true
      },
      description: {
        type: String,
        required: true
      },
      imgUrl: {
        type: String,
        required: false
      }
    }
  ]
});

// Export the model.
module.exports = mongoose.model('question', QuestionSchema);