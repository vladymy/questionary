// Initialize the express framework
var express 	 	= require('express'),
	mongoose		= require('mongoose'),
	bodyParser		= require('body-parser'),
	methodOverride = require('method-override'),
	_ = require('lodash'),
	databaseName	= 'angular_mongodb';

// Express setup 
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride('X-HTTP-Method-Override'));

// CORS Support
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

mongoose.connect('mongodb://localhost/' + databaseName);

var db = mongoose.connection;
db.on('error', console.error);
db.once('open', function() {
  
  // Load the models.
  app.models = require('./models/index');
// console.log(app.models);
  // Load the routes.
  var routes = require('./routes');
  _.each(routes, function(controller, route) {
    app.use(route, controller(app, route));
  });

	console.log('Listening on port 3000...');
  app.listen(3000);
});